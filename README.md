Copyright (C) 2016-2020 PRISM Development Team

PRISM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

PRISM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with PRISM. If not, see <http://www.gnu.org/licenses/>.


----------------------------------------
1. INTRODUCTION
----------------------------------------


PRISM is a forest management decision support system. The name stands for: Plan-level foRest actIvity Scheduling Model.


PRISM is currently in Alpha version of development. For more information on how to try with the current version, please contact PRISM development team.


Development team:

	Dung Nguyen, Key Developer - June 2016, PostDoctoral Fellow at Colorado State University (USA), dzung.csu@gmail.com
	Yu Wei, Principal Investigator - June 2016, Professor at Colorado State university (USA), yu.wei@colostate.edu
	Eric Henderson, Collaborator - June 2016, Regional Planning Analyst at Forest Service - R1 Northern Region (USA), ehenderson@fs.fed.us
	David Anderson, Collaborator - June 2016, Regional Planning Analyst at Forest Service - R3 Southwestern Region (USA), davidanderson@fs.fed.us
	Kira Deming, Developer - July 2018, MS Student at Colorado State University (USA), kira.deming@rams.colostate.edu

PRISM is licensed under the terms of the GNU General Public License version 3 or later. A copy of this license (license-GPL.txt) could be found in the distribution of PRISM.


PRISM integrates the following open-sources:

	LPSOLVE 5.5.2.5, licensed under the terms of the GNU Lesser General Public License. A copy of this license (license-LGPL.txt) could be found in the distribution of PRISM.
	JFREECHART 1.0.19, licensed under the terms of the GNU Lesser General Public License. A copy of this license (license-LGPL.txt) could be found in the distribution of PRISM.
	JCOMMON 1.0.23, licensed under the terms of the GNU Lesser General Public License. A copy of this license (license-LGPL.txt) could be found in the distribution of PRISM.
	SQLITE 3.27.2.1, licensed under the terms of the Apache License version 2.0. A copy of this license (license-AL.txt) could be found in the distribution of PRISM.
	TABLEFILTER 5.3.1, licensed under the terms of the MIT license. A copy of this license (license-MIT.txt) could be found in the distribution of PRISM.


PRISM text-based logos are designed at http://www.picturetopeople.org, licensed: Free (not limited by any kind of license)
	

PRISM also uses icons from different sources to build its GUI. Below are details of all the icon sets that we are grateful to give credits to:

	"Marmalade" designed by "Icojam" at http://www.icojam.com, downloaded at https://www.iconfinder.com/iconsets/marmalade, license: Free for commercial use
	"Vista Style Base Software" designed by "Icons Land" at http://www.icons-land.com, downloaded at https://www.iconfinder.com/iconsets/softwaredemo, license: Free for commercial use
	"Icocentre Free Icons" designed by "IcoCentre" at https://www.iconfinder.com/konekierto, downloaded at https://www.iconfinder.com/iconsets/icocentre-free-icons, license: Free for commercial use
	"32x32 Free Design Icons" designed by "Aha-Soft" at https://www.iconfinder.com/aha-soft, downloaded at https://www.iconfinder.com/iconsets/32x32-free-design-icons, license: Free for commercial use
	"Iconza" designed by "Turbomilk" at http://www.turbomilk.com, downloaded at https://www.iconfinder.com/iconsets/iconza, license: Free for commercial use
	"Computer hardware" designed by "Mirage Design Studio" at https://www.iconfinder.com/PixelPirate, downloaded at https://www.iconfinder.com/iconsets/computer-hardware, license: Free for commercial use
	"Hosting" designed by "Heart Internet" at https://www.heartinternet.uk, downloaded at https://www.iconfinder.com/iconsets/Hosting_Icons, license: Free for commercial use
	"Mobile Icons" designed by "WebIconSet.com" at http://www.webiconset.com, downloaded at https://www.iconfinder.com/iconsets/Mobile-Icons, license: Free for commercial use
	"Human o2" designed by "Oliver Scholtz (and others)" at https://schollidesign.deviantart.com, downloaded at https://www.iconfinder.com/iconsets/humano2, license: Free for non commercial use
	"Gloss set: Basic" designed by "Momenticons" at http://momentumdesignlab.com, downloaded at https://www.iconfinder.com/iconsets/momenticons-gloss-basic, license: Creative Commons (Attribution 3.0 Unported)
